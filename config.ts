import {IFrontBoxConfig} from "./frontbox/gulp/interface";

export const configVirtualHost: string = 'https://mihstudio.dev';

export const configStyle: IFrontBoxConfig[] = [
//	{
//		name: 'global',
//		files: './src/styles/global.scss',
//		dest: '',
//		watch: [
//			'./src/styles/global.scss',
//			'./src/styles/global/*.scss',
//		],
//	},
	{
		name: 'theme',
		files: './src/styles/theme.scss',
		dest: '',
		watch: [
			'./src/**/*.scss',
		],
	},
	{
		name: 'utilities',
		files: './src/style/utilities.scss',
		dest: 'styles',
		concatWith: 'style',
		watch: ['./src/style/utilities.scss', './src/style/utilities/*.scss'],
	},
]

export const configScript = [
	{
		name: 'main',
		files: './src/scripts/main.ts',
		dest: '',
		watch: [
			'./src/scripts/**/*.ts',
			'./src/**/*.ts',
		],
	},
]

export const configHtml: IFrontBoxConfig[] = [
	{
		name: 'main',
		files: './src/template/*.pug',
		dest: '',
		watch: ['./src/template/*.pug'],
	},
	{
		name: 'include',
		files: './src/template/*.pug',
		dest: '',
		otherTasksImpact: true,
		watch: ['./src/template/includes/*.pug'],
	},
	{
		name: 'extends',
		files: './src/template/*.pug',
		dest: '',
		otherTasksImpact: true,
		watch: ['./src/template/extends/*.pug'],
	},
	{
		name: 'partials',
		files: './src/template/partials/*.pug',
		dest: 'partials',
		otherTasksImpact: true,
		watch: ['./src/template/partials/*.pug'],
	},
	{
		name: 'modals',
		files: './src/template/modals/*.pug',
		dest: 'modals',
		watch: ['./src/template/modals/*.pug'],
	}
]

export const configCopy: IFrontBoxConfig[] = [
	{
		name: 'image',
		files: './src/images/**/*',
		dest: 'images',
		watch: ['./src/images/**/*'],
	},
	{
		name: 'fonts',
		files: './src/fonts/*.{eot,woff2,woff,ttf,svg}',
		dest: 'fonts',
		watch: ['./src/fonts/*.{eot,woff2,woff,ttf,svg}'],
	},
	{
		name: 'other',
		files: './src/other/*',
		dest: 'other',
		watch: ['./src/other/*'],
	},
	{
		name: 'video',
		files: './src/video/*',
		dest: 'video',
		watch: ['./src/video/*'],
	},
	{
		name: 'audio',
		files: './src/audio/*',
		dest: 'audio',
		watch: ['./src/audio/*'],
	},
	{
		name: 'favicons',
		files: './src/favicons/*',
		dest: 'favicons',
		watch: './src/favicons/*'
	}
]
