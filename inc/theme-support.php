<?php
/**
 * WordPress theme supports
 *
 * @package GOGOmedia
 */

add_action(
	'after_setup_theme',
	function () {
		add_theme_support( 'post-thumbnails' );
	}
);

add_action(
	'init',
	function () {
		register_nav_menu( 'nav_main', __( 'Menu: main' ) );
		register_nav_menu( 'nav_top_contact', __( 'Menu: top contact' ) );
		register_nav_menu( 'nav_top_lng', __( 'Menu: top lng' ) );
		register_nav_menu( 'nav_footer', __( 'Menu: footer menu' ) );
	}
);

add_theme_support( 'admin-bar', array( 'callback' => '__return_false' ) );
add_theme_support( 'custom-logo' );
add_theme_support( 'title-tag' );

add_action(
	'init',
	function () {
		if ( empty( $_GET['post'] )
		) {
			return;
		}
		$post = sanitize_text_field( wp_unslash( $_GET['post'] ) );

		if ( isset( $post ) ) {
			$template   = get_post_meta( $post, '_wp_page_template', true );
			$front_page = get_option( 'page_on_front' );
			if ( ( 'default' === $template ) && ( $front_page !== $post ) ) {
				return;
			} else {
				remove_post_type_support( 'page', 'editor' );
			}
		}
	}
);

if ( function_exists( 'acf_add_options_page' ) ) {
	acf_add_options_page(
		array(
			'page_title' => 'Theme Settings',
			'menu_title' => 'Theme Settings',
			'menu_slug'  => 'theme-settings',
			'capability' => 'edit_posts',
			'redirect'   => false,
		)
	);
}

add_action(
	'init',
	function() {
		global $wp_rewrite;
		$author_slug             = 'autor';
		$wp_rewrite->author_base = $author_slug;
		$wp_rewrite->flush_rules();
	}
);

add_theme_support( 'custom-logo', array() );


// Integrating Advanced Custom Fields:
add_filter( 'acf/settings/save_json', function() {
    return get_stylesheet_directory() . '/acf-json';
} );
add_filter( 'acf/settings/load_json', function( $paths ) {
    unset( $paths[0] );
    $paths[] = get_template_directory() . '/acf-json';
    $paths[] = get_stylesheet_directory() . '/acf-json';
    return $paths;
} );

add_filter('use_block_editor_for_post', '__return_false', 10);

add_filter( 'timber_context', 'speed_timber_context'  );

function speed_timber_context( $context ) {
	$context['options'] = get_fields('option');
	return $context;
}

remove_action( 'wpcf7_init', 'wpcf7_add_form_tag_submit' );
add_action( 'wpcf7_init', 'new_wpcf7_add_shortcode_submit_button',20 );

function new_wpcf7_add_shortcode_submit_button() {
	wpcf7_add_form_tag( 'submit', 'new_wpcf7_submit_button_shortcode_handler' );
}

function new_wpcf7_submit_button_shortcode_handler( $tag ) {
	$tag = new WPCF7_FormTag( $tag );

	$class = wpcf7_form_controls_class( $tag->type );

	$atts = array();

	$atts['class'] = $tag->get_class_option( $class );
	$atts['id'] = $tag->get_id_option();
	$atts['tabindex'] = $tag->get_option( 'tabindex', 'int', true );

	$value = isset( $tag->values[0] ) ? $tag->values[0] : '';
	if ( empty( $value ) )
		$value = __( 'Send', 'contact-form-7' );

	$atts['type'] = 'submit';

	$atts = wpcf7_format_atts( $atts );

    $html = sprintf( '<button %1$s>%2$s</button>', $atts, $value );

    return $html;
}