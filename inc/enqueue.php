<?php
/**
 * WordPress enqueue functions and styles
 *
 * @package GOGOmedia
 */

/**
 * Enqueue styles
 */
function enqueue_style() {
	// wp_enqueue_style( 'fonts', get_template_directory_uri() . '/public/global.css', false, 1.0 );.
	wp_enqueue_style( 'style', get_template_directory_uri() . '/public/theme.css', false, 1.0 );
}

/**
 * Enqueue scripts
 */
function enqueue_script() {
	$ajax_object = array(
		'ajax_url' => admin_url( 'admin-ajax.php' ),
		'wp_nonce' => wp_create_nonce(),
	);

	// wp_enqueue_script( 'comment-reply' );.
	wp_enqueue_script( 'app', get_template_directory_uri() . '/public/main.js', false, '1.0.0', true );
	// wp_localize_script( 'app', 'ajax_object', $ajax_object );.
}

add_action( 'wp_enqueue_scripts', 'enqueue_style' );
add_action( 'wp_enqueue_scripts', 'enqueue_script' );
