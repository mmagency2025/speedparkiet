/*!
 * FrontPageTimber
 * Michal
 */
require('./frontbox/gulp/assets')

const argv = require('yargs').argv
const del = require('del')

import { Gulpclass, SequenceTask, Task } from 'gulpclass/Decorators'
import { configVirtualHost } from './config'
import { websiteDestinationPath } from './frontbox/gulp/frontbox'
import { FrontBoxGulpScript } from './frontbox/gulp/script'
import { FrontBoxGulpStyle } from './frontbox/gulp/style'

export const browserSync = require('browser-sync').create()

let script: FrontBoxGulpScript
let style: FrontBoxGulpStyle

@Gulpclass()
export class Gulpfile {
	@Task()
	createServer(done) {
		return browserSync.init({
			open: false,
			port: 80,
			proxy: configVirtualHost,
		})
	}

	@Task()
	async cleanWebsite() {
		return del.sync(websiteDestinationPath)
	}

	@SequenceTask()
	website() {
		style = new FrontBoxGulpStyle()
		script = new FrontBoxGulpScript()

		const sequenceTask = [];

		if (argv.clean) {
			sequenceTask.push('cleanWebsite')
		}

		if (argv.generate) {
			sequenceTask.push('buildDevWebsite')
		}

		if (argv.generate && argv.prod) {
			sequenceTask.push('buildProdWebsite')
		}

		if (argv.server) {
			sequenceTask.push('createServer')
		}

		return sequenceTask;
	}

	@Task()
	async buildDevWebsite() {
		await script.start()
		await style.start()
	}

	@Task()
	async buildProdWebsite(done) {
		await style.startProd()
	}
}
