const $ = require('jquery');


/* functions */
function heroslider() {

    const sliderEl = $('.js-hero');

    sliderEl.each(function () {

        const that = $(this);

        $(this).slick({
            dots: false,
            centerMode: true,
            slidesToShow: 1,
            slidesToscroll: 1,
            infinite: true,
            initialSlide: 1,
            autoplay: true,
            autoplaySpeed: 5000,
        });
    });
}

export default heroslider;