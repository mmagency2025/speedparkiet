<?php
/**
 * WordPress functions
 *
 * @package GOGOmedia
 */

// Removing Version Information.
remove_action( 'wp_head', 'wp_generator' );

if ( ! class_exists( 'Timber' ) ) {
	add_action(
		'admin_notices',
		function () {
			echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php' ) ) . '</a></p></div>';
		}
	);

	return;
}

Timber::$dirname = array(
	'src/layouts',
	'src/templates',
	'src/views',
	'src/components',
);

$path_components = 'src/components/';
$path_templates  = 'src/templates/';
$path_sections   = 'src/sections/';
$path_views      = 'src/views/';
$path_posts      = 'inc/posts/';
$path_acf        = 'inc/acf/';

require_once 'inc/enqueue.php';
require_once 'inc/theme-support.php';
require_once 'inc/tinymce.php';

$site = include_once 'inc/class-site.php';

global $site;

$site->initSite();

require_once 'inc/twig-extends.php';

require_once 'src/templates/templates.php';

/**
 * Remove WordPress version.
 */
function remove_wordpress_version() {
	return '';
}
add_filter( 'the_generator', 'remove_wordpress_version' );

/**
 * Pick out the version number from scripts and styles
 *
 * @param string $src Link.
 */
function remove_version_from_style_js( $src ) {
	if ( strpos( $src, 'ver=' . get_bloginfo( 'version' ) ) ) {
		$src = remove_query_arg( 'ver', $src );
	}
	return $src;
}
add_filter( 'style_loader_src', 'remove_version_from_style_js' );
add_filter( 'script_loader_src', 'remove_version_from_style_js' );
